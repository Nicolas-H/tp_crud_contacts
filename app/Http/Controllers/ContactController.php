<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::all();
        return view('contacts.index', ['contacts' => $contacts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->except('_token');
        $contact = new Contact();
        foreach ($inputs as $key => $value) {
            $contact->$key = $value;
        }
        $contact->save();
    
        return redirect(route('contacts.index'))->with('success', 'Contact enregistré');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::find($id);
        return view('contacts.edit', ['contact' => $contact]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->except('_token','_method');
        $contact = Contact::find($id);
        foreach ($inputs as $key => $value) {
            $contact->$key = $value;
        }
        $contact->save(); // sauvegarde les données insérées

        return redirect(route('contacts.index'))->with('success','Experience mis à jour');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();

        return redirect (route('contacts.index'))->with('sucess', 'Experience supprimée avec succès !');
    }

    public function search(Request $request)
    {
        // $search = $request->get('search');
        // $contacts = DB::table('contacts')->where('name', 'like', '%'.$search.'%')->paginate(5);
        // $contacts = DB::table('contacts')->where('lastname', 'like', '%'.$search.'%')->paginate(5);
        $contacts=Contact::all();
        return view('contacts.search', ['contacts'=>$contacts]);
    }
}
