<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Import font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/axentix@0.4.2/dist/css/axentix.min.css" />
  </head>

  <body class="layout">
    <header>
      <nav class="navbar primary">
        <a href="#" target="_blank" class="navbar-brand">TP Crud Contacts</a>
        <div class="navbar-menu ml-auto">
          <a class="navbar-link" href={{route('contacts.index')}}>Mes Contacts</a>
          <a class="navbar-link" href={{route('contacts.create')}}>Créer</a>
          <a class="navbar-link" href={{route('contacts.search')}}>Rechercher</a>
        </div>
      </nav>
    </header>

    <main>
    @yield('contenu')
    </main>

    <footer class="footer primary">
      Copyright © 2019 - Example
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/axentix@0.4.2/dist/js/axentix.min.js"></script>
  </body>
</html>