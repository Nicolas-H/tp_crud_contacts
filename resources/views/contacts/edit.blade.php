@extends('template.app')

@section('title', 'Editer - Contact')

@section('contenu')
<div class="card">
    <div class="card-header">Editer une contact</div>
    <div class="card-content">
        <form class="form-material" method="POST" action="{{route('contacts.update', ['contact' => $contact->id])}}">
            @method('PUT')
            @csrf
            <div class="grix xs1">
                <div class="form-field">
                    <input type="text" id="name" name="name" class="form-control" value="{{$contact->name}}" />
                    <label for="title">Name</label>
                </div>
                <div class="form-field">
                    <label for="lastname">Lastname</label>
                    <textarea id="lastname" name="lastname" class="form-control">{{$contact->lastname}}</textarea>
                </div>
            </div>
            <button type="submit" class="btn press secondary">Modifier</button>
        </form>
    </div>
</div>
@endsection