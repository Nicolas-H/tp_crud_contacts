@extends('template.app')

@section('title', 'Rechercher - Contact')

@section('contenu')
<div class="card">
    <div class="card-header">Rechercher un contact</div>
    <div class="card-content">
        <form class="form-material" method="POST" action="{{route('contacts.update', ['contacts' => $contact->id])}}">
            @method('PUT')
            @csrf
            <div class="grix xs1">
                <div class="form-field">
                    <input type="text" id="name" name="name" class="form-control" value="{{$contact->name}}" />
                    <label for="title">Par prenom</label>
                </div>
                <div class="form-field">
                    <input type="text" id="lastname" name="lastname" class="form-control" value="{{$contact->name}}" />
                    <label for="title">Par nom</label>
                </div>
            </div>
            <button type="submit" class="btn press secondary">Rechercher</button>
        </form>
    </div>
</div>
@endsection