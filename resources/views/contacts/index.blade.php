@extends('template.app')

@section('title', 'Mes contacts')

@section('contenu')
<div class="responsive-table">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>LastName</th>
                <th>Phone</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            @foreach($contacts as $contact)
            <tr>
                <td>{{$contact->id}}</td>
                <td>{{$contact->name}}</td>
                <td>{{$contact->lastname}}</td>
                <td>{{$contact->phone}}</td>
                <td>{{$contact->email}}</td>
                <td>
                <a href="{{route('contacts.edit', ['contact' => $contact->id])}}" class="btn circle secondary">
                            <i class="fas fa-pen"></i>
                </a>
                <form method="POST" action="{{route('contacts.destroy', ['contact' => $contact->id])}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="ml-2 btn circle red dark-1">
                                <i class="fas fa-trash"></i>
                            </button>
                </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
