@extends('template.app')

@section('title', 'Créer - Contact')

@section('contenu')
<div class="card">
    <div class="card-header">Créer un contact</div>
    <div class="card-content">
        <form class="form-material" method="POST" action="{{route('contacts.store')}}">
            @csrf
            <div class="grix xs1">
                <div class="form-field">
                    <input type="text" id="name" name="name" class="form-control" />
                    <label for="title">Name</label>
                </div>
                <div class="form-field">
                    <label for="lastname">Lastname</label>
                    <textarea id="lastname" name="lastname" class="form-control"></textarea> 
                </div>
                <div class="form-field">
                    <label for="phone">Phone</label>
                    <textarea id="phone" name="phone" class="form-control"></textarea> 
                </div>
                <div class="form-field">
                    <label for="email">Email</label>
                    <textarea id="email" name="email" class="form-control"></textarea> 
                </div>
            </div>
            <button type="submit" class="btn press primary">Créer</button>
        </form>
    </div>
</div>
@endsection